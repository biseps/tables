# Interstellar Extinction, Bolometric Correction and Limb Darkening data files #


### Folders 'BC3/' and 'extinct/'

see paper:

Girardi, L., J. Dalcanton, B. Williams, R. de Jong, C. Gallart, M. Monelli, and others, ‘Revised Bolometric Corrections and Interstellar Extinction Coefficients for the ACS and WFPC2 Photometric Systems’, Publications of the Astronomical Society of the Pacific, 120 (2008), 583–91 <http://dx.doi.org/10.1086/588526>


### Folders 'grav2/' and 'ldc2/'

see paper:

Claret, A., and S. Bloemen, ‘Limb-Darkening Coefficients (Claret+, 2011)’, VizieR Online Data Catalog, 352 (2011), 99075
http://adsabs.harvard.edu/abs/2011yCat..35299075C