import numpy as np
import os, errno

   #1-  5  F5.2  [cm/s2] logg     [0/5] Surface gravity
   #7- 12  F6.0  K       Teff     [2000/50000] Effective temperature
  #14- 17  F4.1  [Sun]   Z        [-5/1] Metallicity (log[M/H])
  #19- 22  F4.1  km/s    xi       [0/8] Microturbulent velocity
  #24- 30  F7.4  ---     a        Quadratic limb darkening coefficient a (G2)
  #32- 38  F7.4  ---     b        Quadratic limb darkening coefficient b (G2)
  #40- 41  A2    ---     Filt     Filter (G1)
      #43  A1    ---     Met      [LF] Method (Least-Square or Flux Conservation)
  #45- 51  A7    ---     Mod      Model (ATLAS or PHOENIX)
#----------------------------------------------------------------


	
def read_array(filename,dtype,sep=None):
	"""
	Read a file with an arbitrary number of columns.
	The type of data in each column is arbitrary
	It will be cast to the given dtype at runtime
	"""
	cast = np.cast
	size=len(dtype)
	data = [[] for dummy in xrange(size)]
	with open(filename,'r') as f:
		for line in f:
			fields = line.strip().split(sep)
			for i, number in enumerate(fields):
				if len(number) == 0 or number=='\n':
					number=0
				data[i].append(number)
	for i in xrange(size):
		data[i] = cast[dtype[i]](data[i])
	return np.rec.array(data, dtype=dtype)
	
def write_array(filename,data,fileMode="w"):
	with open(filename,fileMode) as f:
		for i in xrange(np.size(data)):
			f.write(' '.join([`num` for num in data[i]])+"\n")
	f.close()

def mkdir_p(path):
	try:
		os.makedirs(path)
	except OSError as exc: # Python >2.5
		if exc.errno == errno.EEXIST:
			pass
		else: raise

ldc=read_array("raw/tableab.dat",np.dtype([('logg','float'),('teff','float'),('met','float'),('micro','float'),('l1','float'),
('l2','float'),('filt','a2'),('meth','a1'),('model','a7')]))

ldc=ldc[(ldc.micro==2.0)]

uniqMet=np.unique(ldc.met)
     #Kp = Kepler
     #C  = CoRoT
     #S1 = Spitzer filter 1, 3.6um
     #S2 = Spitzer filter 2, 4.5um
     #S3 = Spitzer filter 3, 5.8um
     #S4 = Spitzer filter 4, 8.0um
   #uvby = Stroemgren uvby filters
    #UVBRIJHK = Johnson-Cousins UVBRIJHK filters
  #u'g'r'i'z' = SDSS u'g'r'i'z' filters

filterMap={'B':352,'C':510,'H':356,'I':354,'J':355,'K':357,'Kp':500,'R':353,'S1':320,'S2':321,'S3':322,'S4':323,'U':350,'V':351,'b':333,'g*':311,'i*'
:313,'r*':312,'u':331,'u*':310,'v':332,'y':334,'z*':314}

#print uniqMet
#Make directorys
for met in uniqMet:
	#make a proper name
	if met <0:
		name="m"
	else:
		name="p"
	name=name+str(np.abs(met))
	mkdir_p(name)

	metInd=(ldc.met==met)&(ldc.model=="ATLAS")&(ldc.meth=="L")
	#Split up files based on filter
	for filt,num in filterMap.iteritems():
		ind=metInd&(ldc.filt==filt)
		write_array(name+"/ldc1_header_main.dat",np.rec.array([ldc.teff[ind],ldc.logg[ind]]))
		write_array(name+"/ldc2_header_main.dat",np.rec.array([ldc.teff[ind],ldc.logg[ind]]))
		write_array(name+"/ldc1_"+str(num)+"_main.dat",np.rec.array([ldc.l1[ind]]))
		write_array(name+"/ldc2_"+str(num)+"_main.dat",np.rec.array([ldc.l2[ind]]))

